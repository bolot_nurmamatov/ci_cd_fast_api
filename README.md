# My ci_cd_fast_api Project

This is a FastAPI project with GitLab CI/CD and pytest.

## Additional info of my test task

https://slash-snapper-659.notion.site/Documentation-of-Test-Task-79321d2908044f71970fbf0d3202bfd7?pvs=4

## Installation

--Open terminal

--Create python3.8 venv

python3.8 -m venv venv

source venv/bin/activate

--Install libraries (venv)

pip install pytest fastapi inicorn uvicorn httpx 


## Set up git (venv)

git init

--fork this project to your own repository in GitLab

git clone <YOUR_REPOSITORY_URL>


## Usage

--Run application
--Open terminal

uvicorn app.main:app --reload

## Testing

--Run testing 
--Open terminal

pytest
